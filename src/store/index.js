import Vue from 'vue';
import Vuex from 'vuex';
import createPersistedState from 'vuex-persistedstate';

import categoriesState from './categories';
import keywordSearchState from './keyword-search';
import locationSearchState from './location-search';

Vue.use(Vuex);

const store = new Vuex.Store({
  modules: {
    categoriesState: categoriesState,
    keywordSearchState: keywordSearchState,
    locationSearchState: locationSearchState,
  },
  plugins: [
    createPersistedState(),
  ],
});

export default store;
