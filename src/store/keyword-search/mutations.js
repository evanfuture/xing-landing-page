export default {
  toggleCategoryDropdown(state) {
    state.categoriesOpen = !state.categoriesOpen;
  },
  resetCategoryDropdown(state) {
    state.categoriesOpen = false;
  },
  setUserKeyword(state, keyword) {
    state.userKeyword = keyword;
  },
};
