import CategoriesService from '@/services/categories.service';

export default {
  getCategories() {
    CategoriesService.getCategories().then((categories) => {
      this.commit('setCategories', categories);
    });
  },
};
