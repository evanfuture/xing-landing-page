export default {
  selectedCategoryCount: (state) => {
    if (state.userCategories.length === 1) {
      return '1 category';
    }

    if (state.userCategories.length > 0
      && state.userCategories.length < state.categories.length) {
      return `${state.userCategories.length} categories`;
    }

    return 'all categories';
  },
};
