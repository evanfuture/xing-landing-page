export default {
  setCategories(state, categories) {
    state.categories = categories;
  },
  setUserCategory(state, category) {
    state.userCategories = [...state.userCategories, category];
  },
  unsetUserCategory(state, category) {
    state.userCategories = state.userCategories.filter(cat => cat.id !== category.id);
  },
  resetUserCategories(state) {
    state.userCategories = [];
  },
  toggleAllCategories(state, force = !state.allCategoriesSelected) {
    state.allCategoriesSelected = force;
  },
};
