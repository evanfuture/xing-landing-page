import LocationsService from '@/services/locations.service';

export default {
  getLocationMatches(context, query) {
    LocationsService.getLocationMatches(query)
      .then((locations) => {
        if (locations.length <= 0 || locations[0] === '') {
          throw new Error();
        }
        context.commit('setVisibleLocations', locations);
      })
      .catch(() => {
        context.commit('resetLocations');
      });
  },
};
