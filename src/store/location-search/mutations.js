export default {
  setVisibleLocations(state, locations) {
    state.locations = locations;
  },
  resetLocations(state, userLocation = state.selectedLocation) {
    state.locations = [];
    state.selectedLocation = userLocation;
  },
  setSelectedLocation(state, location) {
    state.selectedLocation = location;
  },
};
