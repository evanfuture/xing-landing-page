import Vue from 'vue';
import Router from 'vue-router';
import LandingPage from '@/views/landing-page/LandingPage';

Vue.use(Router);

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'LandingPage',
      component: LandingPage,
    },
  ],
});
