export default class CategoriesService {
  static getCategories() {
    return fetch('/static/categories.json')
      .then(r => r.json())
      .then(data => data.categories);
  }
}

