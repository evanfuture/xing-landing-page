import fetchJsonp from 'fetch-jsonp';

export default class LocationsService {
  static getLocationMatches(query) {
    return fetchJsonp(`//gd.geobytes.com/AutoCompleteCity?&filter=DE&q=${query}`)
      .then(r => r.json());
  }
}
